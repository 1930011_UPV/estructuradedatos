

public class IntSLLNodo 
{
    public int info;         //tipo de dato a almacenar en el nodo
    public IntSLLNodo next; //direccion de memoria del siguiente nodo
    
    public IntSLLNodo(int i){
        this(i,null);
    }
    
    public IntSLLNodo(int i,IntSLLNodo n){
        info = i;
        next = n;
    }
}
